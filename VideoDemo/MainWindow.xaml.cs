﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Threading;

//using OpenCvSharp;
//using OpenCvSharp.CPlusPlus;

namespace VideoDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private OpenCvSharp.CPlusPlus.VideoCapture capture;
        private DispatcherTimer timer;
        private OpenCvSharp.CPlusPlus.Mat imgBuffer; // Frame image buffer
        private int currentFrame;
        private int totalFrame;

        public MainWindow()
        {
            InitializeComponent();

            this.imgBuffer = new OpenCvSharp.CPlusPlus.Mat();
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "MP4|*.mp4;*.m4v|All files|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                if (this.timer != null)
                {
                    this.timer.Stop();
                }

                this.capture = new OpenCvSharp.CPlusPlus.VideoCapture(openFileDialog.FileName);
                this.currentFrame = 0;

                // Total Frame
                this.totalFrame = capture.FrameCount;
                
                // StatusBar
                this.statusLabel.Content = "Total Frame : " + this.totalFrame + ", FPS : " + capture.Fps;
            }
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (this.totalFrame == 0)
            {
                MessageBox.Show("動画を選んでください。");
                return;
            }

            if (this.timer != null)
            {
                this.timer.Stop();
            }

            if (this.totalFrame > 0)
            {
                if (this.btnPlay.IsChecked == true) // to play
                {
                    this.btnPlay.Content = "Stop";

                    this.currentFrame = 0;

                    this.timer = new DispatcherTimer();
                    this.timer.Interval = TimeSpan.FromMilliseconds(1000 / capture.Fps);
                    this.timer.Tick += timer_Tick;
                    this.timer.Start();
                }
                else // to stop
                {
                    this.btnPlay.Content = "Play";

                }
            }
            else
            {
                this.btnPlay.IsChecked = false;
                this.btnPlay.Content = "Play";
            }
            
        }

        private void catureFrame()
        {
            if (this.currentFrame >= this.totalFrame)
            {
                if (this.timer != null)
                {
                    this.timer.Stop();
                    this.btnPlay.IsChecked = false;
                    this.btnPlay.Content = "Play";
                }
                return;
            }

            capture.Set(OpenCvSharp.CaptureProperty.PosFrames, this.currentFrame);
            capture.Read(this.imgBuffer);
            if (this.imgBuffer.Empty())
            {
                if (this.timer != null)
                {
                    this.timer.Stop();
                    this.btnPlay.IsChecked = false;
                    this.btnPlay.Content = "Play";
                }
                return;
            }

            imgPreview.Source = OpenCvSharp.Extensions.BitmapSourceConverter.ToBitmapSource(this.imgBuffer);

            this.currentFrame++;
            this.statusLabel2.Content = "Current Frame : " + this.currentFrame;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            this.catureFrame();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (this.totalFrame == 0)
            {
                MessageBox.Show("動画を選んでください。");
                return;
            }

            int targetStep = Int32.Parse(this.inputStep.Text);

            if (targetStep <= 0)
            {
                MessageBox.Show("1以上を入力してください。");
                return;
            }

            if (this.comboBoxType.SelectedIndex == 1) // 秒の場合
            {
                targetStep = targetStep * (int)this.capture.Fps;

                if (targetStep > this.totalFrame)
                {
                    MessageBox.Show("1 ~ " + (int)(this.totalFrame / this.capture.Fps) + " 秒の範囲で入力してください。");
                    return;
                }
            }
            else
            {
                if (targetStep > this.totalFrame)
                {
                    MessageBox.Show("1 ~ " + this.totalFrame + " フレームの範囲で入力してください。");
                    return;
                }
            }
            
            var saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            saveFileDialog.Filter = "Jpeg|*.jpg";
            if (saveFileDialog.ShowDialog() == true)
            {
                string prefix = saveFileDialog.FileName.Substring(0, saveFileDialog.FileName.Length - 4);
                int count = 0;

                while ((count * targetStep) < this.totalFrame)
                {
                    capture.Set(OpenCvSharp.CaptureProperty.PosFrames, (count * targetStep));
                    capture.Read(this.imgBuffer);
                    if (this.imgBuffer.Empty())
                    {
                        break;
                    }

                    count++;
                    var bitmap = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(this.imgBuffer);
                    bitmap.Save(prefix + "_" + count + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    bitmap.Dispose();
                }



                MessageBox.Show(count + "枚を保存しました。");
            }
        }

        private void btnPreview_Click(object sender, RoutedEventArgs e)
        {
            if (this.totalFrame == 0)
            {
                MessageBox.Show("動画を選んでください。");
                return;
            }

            int targetFrame = Int32.Parse(this.inputFrameNum.Text);
            if (targetFrame > this.totalFrame)
            {
                MessageBox.Show("Total frame is " + this.totalFrame);
                return;
            }


            capture.Set(OpenCvSharp.CaptureProperty.PosFrames, targetFrame - 1);
            capture.Read(this.imgBuffer);
            if (this.imgBuffer.Empty())
            {
                return;
            }

            imgPreview.Source = OpenCvSharp.Extensions.BitmapSourceConverter.ToBitmapSource(this.imgBuffer);
        }
    }
}
